import time
from pyspark import SparkConf, SparkContext
import random
import matplotlib.pyplot as plt
import numpy as np
from scipy.linalg import eig

######## Utilities ########

def progress_bar(i, total, print_length=40, prefix=''):
    progress_ratio = i / total
    progress_length = int(progress_ratio * print_length)
    progress_string = prefix + ' <' + '=' * progress_length + '-' * (print_length - progress_length) + '> ' + "{:.2f}".format(progress_ratio * 100) +'%' + ' '*4
    print(progress_string, end='\r')

# Function for dot product between matrix (j, (i, value)) and vector (i, value)
def matrix_vector_dot_product(matrix_rdd, vector_rdd, num_partitions):
    dot_product_rdd = matrix_rdd.map(lambda x: (x[1][0], (x[0], x[1][1]))).join(vector_rdd) \
                                .map(lambda x: (x[1][0][0], x[1][0][1] * x[1][1])) \
                                .reduceByKey(lambda x, y: x + y).repartition(num_partitions)
    return dot_product_rdd

# Function for dot product between vector and vector (i, value)
def vector_vector_dot_product(vector1_rdd, vector2_rdd, num_partitions):
    dot_product_rdd = vector1_rdd.join(vector2_rdd) \
                                .mapValues(lambda x: x[0]*x[1]) \
                                .map(lambda x: (0, x[1])) \
                                .reduceByKey(lambda x, y: x+y).repartition(num_partitions)
    return dot_product_rdd

def identity_rdd(n, sc):
    return sc.parallelize([(i, (i, 1)) for i in range(n)])

def euclidean_norm(vector_rdd):
    return vector_rdd.map(lambda x: x[1]*x[1]).reduce(lambda x, y: x+y)**.5

# Function to normalize a vector based on its Euclidean norm
def normalize_vector(vector_rdd):
    norm = euclidean_norm(vector_rdd)
    return vector_rdd.mapValues(lambda x: x / norm)

def get_n(rdd):
    return rdd.filter(lambda x: x[0]).reduce(lambda a,b: max(a,b))[0] + 1

def matrix_matrix_dot_product(matrix1_rdd, matrix2_rdd, num_partitions):
    # Rearrange matrix1 to the format (j, (i, value1))
    matrix2_rdd = matrix2_rdd.map(lambda x: (x[1][0], (x[0], x[1][1])))
    
    # Join matrix1 and matrix2 on the common key 
    # Compute the product of value1 and value2 and rearrange to the format ((i, j), value1 * value2)
    # Sum the products for each unique (i, j) pair
    # Map the final matrix to (i, (j, value))
    dot_product_rdd = matrix1_rdd.join(matrix2_rdd) \
                            .map(lambda x: ((x[1][0][0], x[1][1][0]), x[1][0][1] * x[1][1][1])) \
                            .reduceByKey(lambda x, y: x + y) \
                            .map(lambda x: (x[0][0], (x[0][1], x[1]))) \
                            .repartition(num_partitions)

    return dot_product_rdd

def outer_product(vector1_rdd, vector2_rdd):
    return vector1_rdd.cartesian(vector2_rdd).map(lambda x: ((x[0][0], x[1][0]), x[0][1] * x[1][1]))

def transpose_matrix(matrix_rdd):
    return matrix_rdd.map(lambda x: (x[1][0], (x[0], x[1][1])))

def rdd_to_numpy(matrix_rdd, n_i=None, n_j=None):
    
    if n_i == None:
        n_i = get_n(matrix_rdd)
    if n_j == None:
        n_j = n_i
    
    np_array = np.zeros((n_i,n_j))
    for i, (j, value) in matrix_rdd.collect():
        np_array[i][j] = value
        
    return np_array

def numpy_to_rdd(np_matrix, sc):
    
    rdd_array = []
    
    shape_len = len(np_matrix.shape)
    
    if shape_len == 1:
        for i in range(np_matrix.shape[0]):
            rdd_array.append((i, np_matrix[i]))
    
    elif shape_len == 2:
        for i in range(np_matrix.shape[0]):
            for j in range(np_matrix.shape[1]):
                rdd_array.append((i,(j, np_matrix[i][j])))
                
        
    else:
        raise Exception(f"numpy matrix must be either shape length 1 or 2. This one has shape {np_matrix.shape}")
    
    matrix_rdd = sc.parallelize(rdd_array)
    
    return matrix_rdd

def apply_shift(matrix_rdd, shift):
    # Create a shifted matrix RDD
    matrix_shifted_rdd = matrix_rdd.map(lambda x: (x[0], (x[1][0], x[1][1] - shift if x[0] == x[1][0] else x[1][1])))
    return matrix_shifted_rdd

def shift_eigenvalues(eigvals_T_shifted, shift_value):
    eigvals_original = eigvals_T_shifted + shift_value
    return eigvals_original

def compute_residual_norm(matrix_rdd, eigval, eigvec_rdd, num_partitions):
    Av_rdd = matrix_vector_dot_product(matrix_rdd, eigvec_rdd, num_partitions)
    lambda_v_rdd = eigvec_rdd.mapValues(lambda x: eigval * x)
    residual_rdd = Av_rdd.join(lambda_v_rdd).mapValues(lambda x: x[0] - x[1])
    residual_norm = euclidean_norm(residual_rdd)
    return residual_norm

def random_symmetric_tridiagonal_matrix(sc, n):
    # Create a list to store the matrix entries
    entries = []

    for i in range(n):
        # Diagonal elements
        value = random.random()
        entries.append((i, (i, value)))

        # Upper diagonal elements
        if i < n-1:
            value = random.random()
            entries.append((i, (i+1, value)))
            entries.append((i+1, (i, value)))  # Symmetric lower diagonal elements

    # Create an RDD from the list of entries
    matrix_rdd = sc.parallelize(entries)

    return matrix_rdd

def random_symmetric_matrix(sc, n, density):
    # Create a list to store the matrix entries
    entries = []

    for i in range(n):
        for j in range(i, n):
            # Diagonal elements
            if i == j:
                value = random.random()
                entries.append((i, (j, value)))
            # Off-diagonal elements
            elif random.random() < density:
                value = random.random()
                entries.append((i, (j, value)))
                entries.append((j, (i, value)))  # Symmetric elements

    # Create an RDD from the list of entries
    matrix_rdd = sc.parallelize(entries)

    return matrix_rdd

def random_laplacian_matrix(sc, n, density):
    # Create a list to store the matrix entries
    entries = []

    for i in range(n):
        degree = 0
        for j in range(n):
            if i == j:
                continue  # Skip diagonal elements for now
            
            # Off-diagonal elements
            if random.random() < density:
                value = -1
                degree += abs(value)
                entries.append((i, (j, value)))
                entries.append((j, (i, value)))  # Symmetric elements

        # Diagonal elements
        entries.append((i, (i, degree)))

    # Create an RDD from the list of entries
    matrix_rdd = sc.parallelize(entries)

    return matrix_rdd

######## LANCZOS METHOD ########

# Lanczos Method implementation
def tridiagonalize_lanczos(matrix_rdd, num_partitions, k=10, v_i_rdd=None, return_numpy_T=False, random_seed=None):
    
    if random_seed is not None:
        np.random.seed(random_seed)
    
    n = get_n(matrix_rdd)
    
    sc = matrix_rdd.context
    
    sc.setCheckpointDir('/tmp/checkpoints/')
    
    if v_i_rdd == None:
        q_rdd = sc.parallelize(np.random.rand(n)).zipWithIndex().map(lambda x: (x[1], x[0])).repartition(num_partitions)
        v_i_rdd = normalize_vector(q_rdd).repartition(num_partitions)
    
    alpha, beta = [], []

    start_time = time.time()
    iterations = min(k, n)
    for i in range(iterations):
        w_i_rdd = matrix_vector_dot_product(matrix_rdd, v_i_rdd, num_partitions).cache()
        alpha_i = vector_vector_dot_product(v_i_rdd, w_i_rdd, num_partitions).reduce(lambda x, y: x+y)[1]
        w_i_rdd = w_i_rdd.join(v_i_rdd).mapValues(lambda x: x[0] - (x[1] * alpha_i)).repartition(num_partitions).cache()

        if i > 0:
            w_i_rdd = w_i_rdd.join(v_im1_rdd).mapValues(lambda x: x[0] - (x[1] * beta_i)).repartition(num_partitions).cache()
            
        beta_i = euclidean_norm(w_i_rdd)
        if beta_i == 0:
            break

        alpha.append(alpha_i)
        beta.append(beta_i)
        
        v_i_rdd.checkpoint()

        v_im1_rdd = v_i_rdd
        v_i_rdd = w_i_rdd.mapValues(lambda x: x / beta_i).cache()
        
        # time and progress
        total_time = time.time() - start_time
        avg_iteration = total_time / (i+1)
        time_left = avg_iteration * (iterations- i) /60
        progress_bar(i+1, iterations, prefix=f'step:{i+1}, Avg. Iteration: {avg_iteration:.2f} s, Time Left: {time_left:.2f} min.')
        
        if i == 0:
            lanczos_vectors_rdd = v_i_rdd.map(lambda x: (0, (x[0], x[1])))
            
        else:
            lanczos_vectors_rdd = lanczos_vectors_rdd.union(v_i_rdd.map(lambda x: (i, (x[0], x[1]))))
                    

    tridiagonal = np.diag(alpha) + np.diag(beta[:-1], k=1) + np.diag(beta[:-1], k=-1)
    
    if not return_numpy_T:
        tridiagonal = numpy_to_rdd(tridiagonal, sc)
    
    return tridiagonal, lanczos_vectors_rdd



def eig_lanczos_rdd(matrix_rdd, num_partitions, k=10, num_restarts=1, random_seed=None, v_i_rdd=None):
    
    """ Eigensolver for Pyspark RDD's """
        
    sc = matrix_rdd.context

    for r in range(num_restarts):
        if r == 0:
            T, lanczos_rdd = tridiagonalize_lanczos(matrix_rdd, 
                                                    num_partitions=num_partitions, 
                                                    k=k,
                                                    v_i_rdd=v_i_rdd,
                                                    random_seed=random_seed, 
                                                    return_numpy_T=True)
        else:
            residual_norm = compute_residual_norm(matrix_rdd, sorted(eigvals_T)[0], v_i_rdd, num_partitions)
            print('\nRestart:', r, 'Convergence:', residual_norm, '\n')  
            
#             shift_value = eigvals_T[0]
#             matrix_shifted = apply_shift(matrix_rdd, shift_value)

            T, lanczos_rdd = tridiagonalize_lanczos(matrix_rdd, 
                                                    num_partitions=num_partitions, 
                                                    k=k,
                                                    v_i_rdd=v_i_rdd,
                                                    random_seed=random_seed,
                                                    return_numpy_T=True)

        eigvals_T, eigvecs_T = eig(T)

        eigvals_T, eigvecs_T = eigvals_T.real, eigvecs_T.real
        
        index_map = {k: v for k, v in zip(np.argsort(eigvals_T), range(len(eigvals_T)))}
        
        eigvals_A = eigvals_T[np.argsort(eigvals_T)]
        
#         if r > 0:
#             eigvals_A = shift_eigenvalues(eigvals_A, shift_value)

        eigvecs_T_rdd = numpy_to_rdd(eigvecs_T, sc)

        eigvals_A_rdd = numpy_to_rdd(eigvals_A, sc)
        
        eigvecs_A_rdd = matrix_matrix_dot_product(lanczos_rdd, eigvecs_T_rdd, num_partitions) \
                        .mapValues(lambda x: (index_map[x[0]], x[1]))
        
        v_i_rdd = eigvecs_A_rdd.filter(lambda x: x[1][0] == 0).map(lambda x: (x[0], x[1][1]))
    
    return eigvals_A_rdd, eigvecs_A_rdd

########### ARNOLDI METHOD ################

def arnoldi_iteration(matrix_rdd, num_partitions, k=10, v_i_rdd=None, return_numpy_H=False, random_seed=None):

    if random_seed is not None:
        np.random.seed(random_seed)

    n = get_n(matrix_rdd)

    sc = matrix_rdd.context

    sc.setCheckpointDir('/tmp/checkpoints/')

    if v_i_rdd is None:
        Q_rdd = sc.parallelize(np.random.rand(n)).zipWithIndex().map(lambda x: (x[1], x[0])).repartition(num_partitions)
        Q_rdd = normalize_vector(Q_rdd).map(lambda x: (x[0], (0, x[1]))).repartition(num_partitions)
    else:
        Q_rdd = v_i_rdd.map(lambda x: (x[0], (0, x[1])))


    H = np.zeros((k + 1, k))
    
    start_time = time.time()

    for j in range(k):
        q_j_rdd = Q_rdd.filter(lambda x: x[1][0] == j).mapValues(lambda x: x[1])
        w_j_rdd = matrix_vector_dot_product(matrix_rdd, q_j_rdd, num_partitions).cache()
        for i in range(j + 1):
            q_i_rdd = Q_rdd.filter(lambda x: x[1][0] == i).map(lambda x: (x[0], x[1][1]))
            h_ij = vector_vector_dot_product(q_i_rdd, w_j_rdd, num_partitions).reduce(lambda x, y: x + y)[1]
            H[i, j] = h_ij
            w_j_rdd = w_j_rdd.join(q_i_rdd).mapValues(lambda x: x[0] - x[1] * h_ij).repartition(num_partitions).cache()

        h_j_plus_1_j = euclidean_norm(w_j_rdd)
        H[j + 1, j] = h_j_plus_1_j

        if h_j_plus_1_j == 0:
            break
        
        if j < k - 1:
            Q_rdd = Q_rdd.union(w_j_rdd.mapValues(lambda x: (j+1, x / h_j_plus_1_j))).cache()
        
        # time and progress
        total_time = time.time() - start_time
        avg_iteration = total_time / (i+1)
        time_left = avg_iteration * (k - i) /60
        progress_bar(i+1, k, prefix=f'step:{i+1}, Avg. Iteration: {avg_iteration:.2f} s, Time Left: {time_left:.2f} min.')

    if not return_numpy_H:
        H = numpy_to_rdd(H, sc)

    return H, Q_rdd.map(lambda x: (x[1][0], (x[0], x[1][1])))

def eig_arnoldi_rdd(matrix_rdd, num_partitions, k=10, num_restarts=1, random_seed=None, v_i_rdd=None):

    """ Eigensolver for Pyspark RDD's using the Arnoldi method """

    sc = matrix_rdd.context

    for r in range(num_restarts):
        H, arnoldi_rdd = arnoldi_iteration(matrix_rdd,
                                               num_partitions=num_partitions,
                                               k=k,
                                               v_i_rdd=v_i_rdd,
                                               random_seed=random_seed,
                                               return_numpy_H=True)

        eigvals_H, eigvecs_H = eig(H[:-1, :])

        eigvals_H, eigvecs_H = eigvals_H.real, eigvecs_H.real

        index_map = {k: v for k, v in zip(np.argsort(eigvals_H), range(len(eigvals_H)))}

        eigvals_A = eigvals_H[np.argsort(eigvals_H)]

        eigvecs_H_rdd = numpy_to_rdd(eigvecs_H, sc)

        eigvals_A_rdd = numpy_to_rdd(eigvals_A, sc)
        

        eigvecs_A_rdd = matrix_matrix_dot_product(arnoldi_rdd, eigvecs_H_rdd, num_partitions) \
                        .mapValues(lambda x: (index_map[x[0]], x[1]))
        
        v_i_rdd = eigvecs_A_rdd.filter(lambda x: x[1][0] == 0).map(lambda x: (x[0], x[1][1]))

    return eigvals_A_rdd, eigvecs_A_rdd
        