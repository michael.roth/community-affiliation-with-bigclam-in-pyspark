# Import necessary modules
from neo4j import GraphDatabase
from graphframes import GraphFrame
from pyspark.sql import SparkSession
from pyspark import SparkConf, SparkContext
import numpy as np
from pyspark.sql import Column
from pyspark.sql.functions import col, udf
from pyspark.sql.types import IntegerType, StructType, StructField, DoubleType, ArrayType, StringType
import pyspark.sql.functions as F
import time
from src.pyspark_eigensolver import *

def create_default_neo4j_spark_session(uri, user, password):

    # Create a SparkConf object with the desired configurations
    conf = SparkConf().setAppName('eigs_solver') \
                .set('spark.driver.memory', '6g') \
                      .set('spark.executor.memory', '6g') \
                      .set('spark.executor.cores', '8') \
                      .set('spark.rdd.compress', 'True')


    # Create a SparkSession with the Neo4j configuration and the SparkConf object
    spark = SparkSession.builder.appName('eigs_solver') \
        .config('spark.jars.packages', 'org.neo4j:neo4j-connector-apache-spark_2.12:5.0.0_for_spark_3,graphframes:graphframes:0.8.0-spark3.0-s_2.12') \
        .config('spark.neo4j.bolt.url', uri) \
        .config('spark.neo4j.bolt.user', user) \
        .config('spark.neo4j.bolt.password', password) \
        .config(conf=conf) \
        .getOrCreate()
    
    return spark

def create_sample_adj_matrix(spark, uri, user, password):

    # Query for the vertices that match the filter
    vertices = spark.read.format("org.neo4j.spark.DataSource") \
        .option("url", uri) \
        .option("authentication.basic.username", user) \
        .option("authentication.basic.password", password) \
        .option('query','''MATCH (n)
                            RETURN id(n) as id''') \
        .load()

    # Query for the edges that connect the specified vertices
    edges = spark.read.format("org.neo4j.spark.DataSource") \
        .option("url", uri) \
        .option("authentication.basic.username", user) \
        .option("authentication.basic.password", password) \
        .option("query", """MATCH (n1)--(n2)
                            WITH id(n1) as src, id(n2) as dst
                            RETURN src, dst""") \
        .load()

    # Create a GraphFrame object using the vertices and edges DataFrame
    graph = GraphFrame(vertices, edges)

    # Create a variable edge_data that contains the graph.edges data
    edge_data = graph.edges

    # Construct the adjacency matrix as a DataFrame
    adjacency_matrix = edge_data.selectExpr("src as i", "dst as j", "1 as value") \
                                .distinct().sort('i', 'j')

    # Construct the degree matrix as a DataFrame
    degree_matrix = adjacency_matrix.groupby('i') \
                                    .sum('value') \
                                    .withColumn('j', col('i')) \
                                    .select('i', 'j', 'sum(value)') \
                                    .withColumnRenamed('sum(value)', 'value') \
                                    .sort('i')

    # Get n number of total nodes in the complete graph
    n = degree_matrix.selectExpr('MAX(i) AS n').first().n + 1

    # Compute the Laplacian matrix as a RDD
    laplacian_matrix = degree_matrix.union(adjacency_matrix) \
                .withColumn("laplacian", col("value") * (F.when(col('i') == col('j'), 1).otherwise(-1))) \
                .select('i', 'j', 'laplacian')

    num_partitions = 1

    adjacency_rdd = adjacency_matrix.rdd.map(lambda x: (x.i, (x.j, x.value))).repartition(num_partitions)

    return adjacency_rdd